package sample

import java.io.{BufferedReader, File, FileInputStream, InputStreamReader}

import scala.collection.mutable.ListBuffer

/**
 * ログファイルを読み込み出力
 * 部分適用、カリー化
 */
object SampleMain {

  def main(args: Array[String]) {

    try {
      val reader : SampleLogReadFile = new SampleLogReadFile()

      //部分適用
      val readStandard = reader.read("c:/work/log/server.log", _:String)
      readStandard("utf-8").foreach(println)

      //カリー化
      val reaByFile = reader.read.curried
      val readByEnc = reaByFile("c:/work/log/server.log")
      readByEnc("utf-8").foreach(println)

    } catch {
      case e : Exception => e.printStackTrace()
    }
  }
}
class SampleLogReadFile {

  /**
   * ファイルを読み込み、読み込んだ内容をList化する関数
   */
  val read = (fileName: String, enc: String) => {

    //mutable (変更可能) な List
    var list = new ListBuffer[String]
    //File読み込み（いったんJavaのパッケージで）
    val reader = new BufferedReader( new InputStreamReader(new FileInputStream( new File( fileName ) ), enc ) )
    var line : String = null

    try {

      while ( { line = reader.readLine; line != null }) {
        list += (line)
        line = reader.readLine
      }
    } finally {
      reader.close
    }
    list.toList
  } : List[String]
}
