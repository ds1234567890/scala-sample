package sample3

import java.io._

import scala.collection.mutable.ListBuffer

/**
 * ログファイルを読み込み特定のレベルを出力する。
 * 関数型
 */
object SampleFunctionMain {

  def main(args: Array[String]) {

    try {
      val reader: SampleFunctionLogReadFile = new SampleFunctionLogReadFile()
      val reaByFile = reader.read.curried
      //出力
      reaByFile("c:/work/log/server.log")("utf-8").filter(str => str.contains(LogLevel.DEBUG.name)).foreach(println)

    } catch {
      case e: Exception => e.printStackTrace()
    }
  }
}

/**
 * ログレベル
 * scalaのenumが使いづらいのでcase object実装
 * @param level
 */
sealed abstract class LogLevel(val level:String) {
  val name = toString
}
object LogLevel {
  case object TRACE extends LogLevel("TRACE")
  case object DEBUG extends LogLevel("DEBUG")
  case object INFO extends LogLevel("INFO")
  case object WARN extends LogLevel("WARN")
  case object ERROR extends LogLevel("ERROR")
}

class SampleFunctionLogReadFile {

  /**
   * ファイルを読み込み、読み込んだ内容をList化する関数
   */
  val read = (fileName: String, enc: String) => {

    //mutable (変更可能) な List
    var list = new ListBuffer[String]
    //File読み込み（いったんJavaのパッケージで）
    val reader = new BufferedReader( new InputStreamReader(new FileInputStream( new File( fileName ) ), enc ) )
    var line : String = null

    try {

      while ( { line = reader.readLine; line != null }) {
        list += (line)
        line = reader.readLine
      }
    } finally {
      reader.close
    }
    list.toList
  } : List[String]
}

