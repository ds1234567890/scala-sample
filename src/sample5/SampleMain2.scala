package sample5

import java.io.{BufferedReader, File, FileInputStream, InputStreamReader}

import scala.collection.mutable.ListBuffer

/**
 * ログファイルを読み込み出力
 * 基礎復習
 */
object SampleMain2 {

  def main(args: Array[String]) {

    try {
      val reader : SampleLogReadFile2 = new SampleLogReadFile2()
      reader.read().foreach(println)

    } catch {
      case e : Exception => e.printStackTrace()
    }
  }
}
class SampleLogReadFile2(fileName: String, enc: String) {

  /**
   * コンストラクタ
   * @return
   */
  def this() = this("c:/work/log/server.log","utf-8")

  /**
   * ファイルを読み込み、読み込んだ内容をList化する関数
   */
  val read = () => {

    //mutable (変更可能) な List
    var list = new ListBuffer[String]
    //File読み込み（いったんJavaのパッケージで）
    val reader = new BufferedReader( new InputStreamReader(new FileInputStream( new File( fileName ) ), enc ) )
    var line : String = null

    try {

      while ( { line = reader.readLine; line != null }) {
        list += (line)
        line = reader.readLine
      }
    } finally {
      reader.close
    }
    list.toList
  } : List[String]
}
