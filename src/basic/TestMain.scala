package basic

/**
 */
object TestMain {

  /**
   * メイン関数
   * @param args
   */
  def main (args: Array[String]) {

    var t1:ClassTest = new ClassTest();
    t1.test1();

    ObjectTest.test1();

    var t2:TraitTest = new TraitSubTest();
    t2.test1();
  }


}
