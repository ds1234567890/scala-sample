package sample2

import java.io.{BufferedReader, File, FileInputStream, InputStreamReader}

import scala.collection.mutable.ListBuffer

/**
 * ログファイルを読み込み特定のレベルを出力する。
 * 非関数型(Javaぽい書き方）
 */
object SampleObjectMain {

  def main(args: Array[String]) {

    try {
      val reader : SampleObjectLogReadFile = new SampleObjectLogReadFile()

      //ファイルを読み込みリスト化
      val readList: List[String] = reader.read("c:/work/log/server.log", "utf-8", LogLevel.DEBUG);

      //出力
      for(str: String <- readList) {
        println(str)
      }

    } catch {
      case e : Exception => e.printStackTrace()
    }
  }
}

/**
 * ログレベル
 * scalaのenumが使いづらいのでcase object実装
 * @param level
 */
sealed abstract class LogLevel(val level:String) {
  val name = toString
}
/** コンパニオンオブジェクトを使用したcase object */
object LogLevel {
  case object TRACE extends LogLevel("TRACE")
  case object DEBUG extends LogLevel("DEBUG")
  case object INFO extends LogLevel("INFO")
  case object WARN extends LogLevel("WARN")
  case object ERROR extends LogLevel("ERROR")
}

class SampleObjectLogReadFile {

  /**
   * ファイルを読み込み、読み込んだ内容をList化する関数
   */
  def read (fileName: String, enc: String, level: LogLevel) : List[String] = {

    //mutable (変更可能) な List
    var list = new ListBuffer[String]
    //File読み込み（いったんJavaのパッケージで）
    val reader = new BufferedReader( new InputStreamReader(new FileInputStream( new File( fileName ) ), enc ) )
    var line : String = null

    try {

      while ( { line = reader.readLine; line != null }) {
        if (line.contains(level.name)) {
          list += (line)
        }
        line = reader.readLine
      }
    } finally {
      reader.close
    }
    list.toList
  }
}
