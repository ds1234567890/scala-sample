package sample4

import java.io._

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
 * ログファイルをレベルごとに出してみる。
 * 非同期処理サンプル
 */
object SampleThreadMain {

  def main(args: Array[String]) {

    try {
      val log : LogReadWriter = new LogReadWriter()
      //読み込み
      val logReader = log.read.curried
      val readList : List[String] = logReader("c:/work/log/server.log")("utf-8")

      //書き込み(パラレル）
      val logWriter = log.write.curried
      val infoWriter : Future[Unit] = Future { logWriter("c:/work/log/info.log")(readList.filter( str =>  str.contains("INFO") )) }
      val debugWriter : Future[Unit] = Future { logWriter("c:/work/log/debug.log")(readList.filter( str =>  str.contains("DEBUG") )) }
      Await.result(infoWriter, Duration.Inf)
      Await.result(debugWriter, Duration.Inf)

    } catch {
      case e : Exception => e.printStackTrace()
    }
  }
}
class LogReadWriter {

  /**
   * ファイルを読み込み、読み込んだ内容をList化する関数
   */
  val read = (fileName: String, enc: String) => {

    //mutable (変更可能) な List
    var list = new ListBuffer[String]
    //File読み込み（いったんJavaのパッケージで）
    val reader = new BufferedReader( new InputStreamReader(new FileInputStream( new File( fileName ) ), enc ) )
    var line : String = null

    try {

      while ( { line = reader.readLine; line != null }) {
        list += (line)
        line = reader.readLine
      }
    } finally {
      reader.close
    }
    list.toList
  } : List[String]

  /**
   * List内容をファイルに出力する関数
   */
  var write = (fileName: String, list: List[String]) => {

    val writer = new PrintWriter(fileName)
    try {
      for (str: String <- list) { writer.println(str) }
      Thread.sleep(5000)
    } finally {
      writer.close
    }
  } :Unit
}
