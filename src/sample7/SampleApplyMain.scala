package sample7

import java.io._

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
 * ログファイルをレベルごとに出してみる。
 * クロージャは引数以外の変数を、定義したその時に設定されたものを利用すると保証できている
 */
object SampleApplyMain {

  def main(args: Array[String]) {

    try {
      val reader : LogReader = new LogReader("c:/work/log/server.log", "utf-8")
      val writer : LogWriter = new LogWriter()
      val warnfilter : Filter = WarnFilter("c:/work/log", "warnFilter.log")
      val infofilter : Filter = InfoFilter("c:/work/log", "infoFilter.log")
      val warnWriter = writer.levelWriter(warnfilter)
      val infoWriter = writer.levelWriter(infofilter)
      val readList : List[String] = reader.read()

      //書き込み(パラレル）
      val warnWriterList : Future[Unit] = Future { warnWriter(readList) }
      val infoWriterList : Future[Unit] = Future { infoWriter(readList) }
      Await.result(warnWriterList, Duration.Inf)
      Await.result(infoWriterList, Duration.Inf)

      warnfilter match {
        //抽出した値を変数dirとfileNameに束縛する。
        case WarnFilter(dir, fileName) => {
          nullCheck(dir)
          nullCheck(fileName)
          println(dir + "/" + fileName)
        }
      }

      infofilter match {
        //抽出した値を変数dirとfileNameに束縛する。
        case InfoFilter(dir, fileName) => {
          nullCheck(dir)
          nullCheck(fileName)
          println(dir + "/" + fileName)
        }
      }

    } catch {
      case e : Exception => e.printStackTrace()
    }
  }

  val nullCheck = (option: Option[String]) => {
    option match {
      case Some(s) => println(s)
      case None => println("null")
    }
  }
}

/**
 * ログレベル
 * scalaのenumが使いづらいのでcase object実装
 * @param level
 */
sealed abstract class LogLevel(val level:String) {
  val name = toString

}
/** コンパニオンオブジェクトを使用したcase object */
object LogLevel {
  case object TRACE extends LogLevel("TRACE")
  case object DEBUG extends LogLevel("DEBUG")
  case object INFO extends LogLevel("INFO")
  case object WARN extends LogLevel("WARN")
  case object ERROR extends LogLevel("ERROR")
  val list = List(LogLevel.TRACE, LogLevel.DEBUG, LogLevel.INFO, LogLevel.WARN, LogLevel.ERROR)
}

/** Logのリーダー */
class LogReader(fileName: String, enc: String) {

  /**
   * コンストラクタ
   * @return
   */
  def this() = this("c:/work/log/server.log","utf-8")

  /**
   * ファイルを読み込み、読み込んだ内容をList化する関数
   */
  val read = () => {

    //mutable (変更可能) な List
    var list = new ListBuffer[String]
    //File読み込み（いったんJavaのパッケージで）
    val reader = new BufferedReader( new InputStreamReader(new FileInputStream( new File( fileName ) ), enc ) )
    var line : String = null

    try {

      while ( { line = reader.readLine; line != null }) {
        list += (line)
        line = reader.readLine
      }
    } finally {
      reader.close
    }
    list.toList
  } : List[String]
}

/** Logのライター */
class LogWriter {

  /**
   * Levelとマッチングする関数
   */
  val matchLevel = (levelList: List[LogLevel], str: String) => {
    var ret : Boolean = false
    for(level <- levelList) {
      if(str.contains(level.name)) {
        ret = true
      }
    }
    ret
  } : Boolean

  /**
   * List内容をファイルに出力する関数
   */
  val write = (fileName: String, list: List[String]) => {

    val writer = new PrintWriter(fileName)
    try {
      for (str: String <- list) { writer.println(str) }
    } finally {
      writer.close
    }
  } :Unit

  /**
   * レベルフィルタに対応した出力関数
   * クロージャぽく書いてみた。
   */
  val levelWriter = (filter : Filter) => {
    val levelFilter = filter;
    (list: List[String]) => {
      write(levelFilter.getName(), list.filter(str => matchLevel(levelFilter.getLevel(), str)))
    }
  }
}

/**
 * フィルターの抽象化
 */
trait Filter {
  def getName() : String
  def getLevel() : List[LogLevel]
}

/**
 * warn用のFilter
 */
class WarnFilter private(val dir : Option[String], val file : Option[String]) extends Filter {
  /**
   * warn以上か判定する関数
   */
  private val isWarn = (levels : LogLevel) => { (levels == LogLevel.WARN || levels == LogLevel.ERROR) } : Boolean

  def getName () = {
    (dir get) + "/" + (file get)
  }

  def getLevel () = {
    for (lvl <- LogLevel.list ; if( isWarn(lvl))) yield lvl
  }
}
/** コンパニオンオブジェクト */
object WarnFilter {
  def apply(dir : String, file : String): Filter = new WarnFilter(Some(dir), Some(file))
  def unapply(filter : WarnFilter) = Option((filter.dir, filter.file))
}

/**
 * Info用のFilter
 */
class InfoFilter private(val dir : Option[String], val file : Option[String]) extends Filter {
  /**
   * info以上か判定する関数
   */
  private val isInfo = (levels : LogLevel) => { (levels == LogLevel.INFO || levels == LogLevel.WARN || levels == LogLevel.ERROR) } : Boolean

  def getName () = {
    (dir get) + "/" + (file get)
  }

  def getLevel () = {
    for (lvl <- LogLevel.list ; if( isInfo(lvl))) yield lvl
  }
}
/** コンパニオンオブジェクト */
object InfoFilter {
  def apply(dir : String, file : String): Filter = new InfoFilter(Some(dir), Some(file))
  def unapply(filter : InfoFilter) = Option((filter.dir, filter.file))
}